import { LightningElement, track, api, wire} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class AccountHeirarchy extends NavigationMixin(LightningElement)  {
    @track showSpinner = false;
    @track accountData = [
            {
                accId:'123',
                accName:'Parent Account',
                billingLocation:'Test',
                active:'Yes',
                isCurrent:false,
                isExpanded:false,
                childAccountsList:[
                    {
                        accId:'124',
                        accName:'Child Account',
                        billingLocation:'Test',
                        active:'Yes',
                        isCurrent:false,
                        isExpanded:false,
                        childAccountsList:[
                            {
                                accId:'125',
                                accName:'Grand Child Account',
                                billingLocation:'Test',
                                active:'Yes',
                                isCurrent:false,
                                isExpanded:false,
                                childAccountsList:[]
                            }
                        ]
                    },
                    {
                        accId:'126',
                        accName:'Second Child Account',
                        billingLocation:'Test',
                        active:'Yes',
                        isCurrent:false,
                        isExpanded:false,
                        childAccountsList:[
                            {
                                accId:'127',
                                accName:'Second Grand Child Account',
                                billingLocation:'Test',
                                active:'Yes',
                                isCurrent:false,
                                isExpanded:false,
                                childAccountsList:[]
                            }
                        ]
                    }
                ]
            }
        ];

    // expand collapse of case related cases
    handleIconClick(event){
        let index =  event.currentTarget.dataset.index;
        this.accountData[index].isExpanded = !(this.accountData[index].isExpanded);
    }
}