import { LightningElement, api, track} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class ChildAccountHeirarchy extends NavigationMixin(LightningElement)  {
    @track childAccountListLocal;

    @api
    get childAccountList() {
        return this.childAccountListLocal;
    }
    set childAccountList(value) {
        this.childAccountListLocal = JSON.parse(JSON.stringify(value));
    }

    // expand collapse of case related cases
    handleIconClick(event){
        let index =  event.currentTarget.dataset.index;
        this.childAccountListLocal[index].isExpanded = !(this.childAccountListLocal[index].isExpanded);
    }

}